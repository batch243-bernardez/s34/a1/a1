const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Mock database

let users = [{
	username: "dmgalang",
	password: "12345678",
}];


// Create a GET route that will access the /home route that will print out a simple message.

	app.get("/home", (request,response) => {
		response.send("Welcome to the home page!");
	});


// Create a GET route that will access the /users route that will retrieve all the users in the mock database.

	app.get("/users", (request,response) => {
		response.send(users);
	});


// Create a DELETE route that will access the /delete-user route to remove a user from the mock database

	app.delete("/delete-user", (request,response) => {
		for(let i = 0; i < users.length; i++){
			if(users[i].username == request.body.username){
				users.splice(i, 1);
				break;
			}
			else{
				response.send(`User ${request.body.username} was not deleted.`);
				break;
			}
		}
		response.send(`User ${request.body.username} was deleted.`);
		console.log(users);
	})


app.listen(port, () => console.log(`Server running at port ${port}`));